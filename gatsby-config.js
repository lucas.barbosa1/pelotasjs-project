require('dotenv').config()
module.exports = {
  siteMetadata: {
    title: 'Pelotas JS',
    description: 'Como implementar seu projeto com o Storyblok'
  },
  plugins: [
    {
      resolve: 'gatsby-transformer-sharp'
    },
    {
      resolve: 'gatsby-source-storyblok',
      options: {
        accessToken: process.env.GATSBY_TOKEN_PUBLIC || process.env.GATSBY_TOKEN_PREVIEW,
        homeSlug: 'home',
        version: process.env.NODE_ENV === 'production' ? 'published' : 'draft'
      }
    },
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/images`
      }
    }
  ]
}
