import Page from './page'
import NavItem from './nav_item'
import Navbar from './navbar'
import Posts from './posts'
import Post from './post'
import Title from './title'
import CustomMessage from './custom_message'
import ComponentNotFound from './component_not_found'

const ComponentList = {
  page: Page,
  nav_item: NavItem,
  navbar: Navbar,
  posts: Posts,
  post: Post,
  title: Title,
  custom_message: CustomMessage
}

const Components = type => {
  if (!ComponentList[type]) {
    return ComponentNotFound
  }

  return ComponentList[type]
}

export default Components
