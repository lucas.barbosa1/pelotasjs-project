import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'

const Post = ({ data: { title, content, author, created_at, avatar }, index, ...props }) => {
  return (
    <Wrapper>
      {title && (
        <StyledLink to={`/post?id=${index}`}>
          <Title>{title}</Title>
        </StyledLink>
      )}
      {content && <Content {...props}>{content}</Content>}
      <Footer>
        {avatar && <Avatar src={avatar} />}
        <div>
          {author && <Author>{`${author},`}</Author>}
          {created_at && <CreatedAt>{created_at}</CreatedAt>}
        </div>
      </Footer>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  max-width: 280px;
  @media (min-width: 480px) {
    max-width: 410px;
  }
  @media (min-width: 768px) {
    max-width: 520px;
  }
  @media (min-width: 1280px) {
    max-width: 700px;
  }
`
const StyledLink = styled(Link)`
  color: #000;
  &:hover {
    text-decoration: none;
  }
`
const Content = styled.span(
  ({ isEntireContent }) => `
  text-align: justify;
  ${!isEntireContent &&
    `overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
    @media (min-width: 768px) {
      -webkit-line-clamp: 3;
    }
    @media (min-width: 1280px) {
      -webkit-line-clamp: 2;
    )`}

`
)
const Footer = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  > div {
    display: flex;
    flex-direction: column;
    margin-left: 20px;
  }
`
const Title = styled.h2`
  font-weight: bold;
`
const Avatar = styled.img`
  max-width: 100px;
  height: auto;
  border-radius: 50%;
`
const Author = styled.span`
  font-weight: bold;
`
const CreatedAt = styled.span`
  font-style: italic;
`
export default Post
