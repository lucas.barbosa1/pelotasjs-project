import React, { useState, useEffect } from 'react'
import Components from './components'
import styled from 'styled-components'
import SbEditable from 'storyblok-react'

import Post from './post'
import Spinner from './spinner'
import Container from './container'
import { getPosts } from '../services/storyblokRequests'

const Page = ({ blok }) => {
  const [postData, setPostData] = useState(null)
  const [isLoading, setIsLoading] = useState(true)
  const [isPostPage, setIsPostPage] = useState(false)

  useEffect(() => {
    const handleGetPosts = async id => {
      try {
        const {
          data: { stories }
        } = await getPosts()
        const posts = stories[0].content.body.find(item => item.component === 'posts').content
        if (posts.length > 0 && id <= posts.length) {
          setPostData(posts.find((item, index) => index === id - 1))
        }
      } catch (err) {
        console.log(err)
      } finally {
        setIsPostPage(true)
        setIsLoading(false)
      }
    }
    // local tests
    if (window.location.pathname === '/editor/') {
      const urlParams = new URLSearchParams(window.location.search)
      if (urlParams.has('path') && urlParams.get('path') === 'post' && urlParams.has('id')) {
        const postId = parseInt(urlParams.get('id'))
        handleGetPosts(postId)
      }
      // builded code
    } else if (window.location.pathname === '/post' || window.location.pathname === '/post/') {
      const urlParams = new URLSearchParams(window.location.search)
      if (urlParams.has('id')) {
        const postId = parseInt(urlParams.get('id'))
        handleGetPosts(postId)
      }
    }
    setIsLoading(false)
  }, [])

  if (isLoading)
    return (
      <CenteredContent>
        <Spinner />
      </CenteredContent>
    )

  return (
    <SbEditable content={blok}>
      <Wrapper>
        {isPostPage ? (
          postData ? (
            <Container marginTop='200'>
              <Post data={postData} isEntireContent />
            </Container>
          ) : (
            <CenteredContent>
              <ErrorText>Invalid token id.</ErrorText>
            </CenteredContent>
          )
        ) : (
          blok.body &&
          blok.body.map(blokBody =>
            React.createElement(Components(blokBody.component), {
              key: blokBody._uid,
              blok: blokBody
            })
          )
        )}
      </Wrapper>
    </SbEditable>
  )
}

const CenteredContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;
`

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const ErrorText = styled.span`
  font-weight: bold;
  font-size: 48px;
  color: red;
`

export default Page
