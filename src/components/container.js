import styled from 'styled-components'

const Container = styled.div(
  ({ marginTop, marginBottom }) => `
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  ${marginTop ? `margin-top: ${marginTop}px;` : ''}
  ${marginBottom ? `margin-bottom: ${marginBottom}px;` : ''}
  padding-left: 15px;
  padding-right: 15px;
  max-width: 290px;

  @media (min-width: 360px) {
    max-width: 320px;
  }

  @media (min-width: 480px) {
    max-width: 420px;
  }

  @media (min-width: 768px) {
    max-width: 708px;
  }

  @media (min-width: 992px) {
    max-width: 932px;
  }

  @media (min-width: 1200px) {
    max-width: 1140px;
  }
`
)

export default Container
