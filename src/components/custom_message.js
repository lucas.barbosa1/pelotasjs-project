import React from 'react'
import styled from 'styled-components'

const CustomMessage = ({ blok: { text } }) => {
  return <Wrapper>{text && <Text>{text}</Text>}</Wrapper>
}

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;
`
const Text = styled.span`
  font-weight: bold;
  font-size: 48px;
`

export default CustomMessage
