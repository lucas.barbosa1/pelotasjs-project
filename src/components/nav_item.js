import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

const NavItem = ({ name, link = {} }) => {
  return <StyledLink to={`/${link.cached_url === 'home' ? '' : link.cached_url}`}>{name}</StyledLink>
}

const StyledLink = styled(Link)`
  color: #000;
  font-size: 20px;
  text-transform: uppercase;
`

export default NavItem
