import React from 'react'
import styled from 'styled-components'

import Container from './container'

const handleFontSize = type => {
  switch (type) {
    case 'large':
      return '72px;'
    case 'medium':
      return '48px;'
    case 'small':
      return '32px;'
    default:
      return '48px;'
  }
}

const Title = ({
  blok: {
    text,
    size,
    color: { color }
  }
}) => {
  return (
    <Container marginTop='120' marginBottom='30'>
      {text && (
        <StyledTitle size={size} color={color || '#000'}>
          {text}
        </StyledTitle>
      )}
    </Container>
  )
}

const StyledTitle = styled.h1(
  ({ size, color }) => `
  font-weight: bold;
  font-size: ${handleFontSize(size)}
  color: ${color};
`
)

export default Title
