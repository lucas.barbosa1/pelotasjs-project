import React from 'react'
import { navigate } from 'gatsby'
import styled from 'styled-components'

import NavItem from './nav_item'
import Button from './button'

const Navbar = ({ blok: { logo, nav_items = [], button } }) => {
  const handleLink = () => {
    navigate('/')
  }

  return (
    <NavbarSection>
      <NavWrapper>
        {logo && <Logo src={logo} onClick={handleLink} />}
        <NavItemsWrapper>
          {nav_items && nav_items.map(item => <NavItem key={item._uid} link={item.link} name={item.name} />)}
        </NavItemsWrapper>
        {button.length && <Button variant={button[0].variant}>{button[0].content}</Button>}
      </NavWrapper>
      <Hr />
    </NavbarSection>
  )
}

const Hr = styled.hr`
  border: 1px solid rgba(0, 0, 0, 0.2);
  width: 100%;
  background-color: #fff;
`
const NavbarSection = styled.section`
  top: 0;
  left: 0;
  width: 100%;
  height: 80px;
  position: fixed;
  padding: 40px 40px 60px 40px;
  background-color: #fff;
`

const NavWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: row;
  flex: 1;
`

const Logo = styled.img`
  max-width: 173px;
  height: auto;
  cursor: pointer;
`

const NavItemsWrapper = styled.div`
  a {
    &:not(:last-child) {
      margin-right: 10px;
    }
  }
`

export default Navbar
