import React from 'react'
import styled from 'styled-components'

import COLORS from '../constants/colors'

const DefaultButton = styled.button(
  ({ full }) => `
  max-width: 100%;
  max-height: 36px;
  line-height: 17px;
  padding: 8px 0;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
  cursor: pointer;
  font-style: normal;
  font-weight: bold;
  transition: all 0.3s;
  text-transform: uppercase;
  border: none;
  height: 36px;
  min-width: 117px;

  &:hover {
    box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
  }

  @media (min-width: 768px) {
    min-width: 149px;
  }

  @media (min-width: 1200px) {
    min-width: 173px;
  }
  ${full ? 'width: 100%;' : ''}
`
)

const PrimaryButton = styled(DefaultButton)`
  background-color: ${COLORS.blue};
  color: white;
`

const PrimaryOutline = styled(DefaultButton)`
  border: 1px solid ${COLORS.blue};
  color: ${COLORS.blue};
  background-color: transparent;
`

const SecondaryOutline = styled(DefaultButton)`
  border: 1px solid ${COLORS.yellow};
  color: ${COLORS.yellow};
  background-color: transparent;
`

const SecondaryButton = styled(DefaultButton)`
  background-color: ${COLORS.yellow};
  color: white;
`

const Button = ({ variant, outline, ...props }) => {
  switch (variant) {
    case 'primary': {
      if (outline) return <PrimaryOutline {...props} />
      return <PrimaryButton {...props} />
    }
    case 'secondary': {
      if (outline) return <SecondaryOutline {...props} />
      return <SecondaryButton {...props} />
    }
    default:
      return <PrimaryButton {...props} />
  }
}

export default Button
