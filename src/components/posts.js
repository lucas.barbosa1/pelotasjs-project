import React from 'react'
import styled from 'styled-components'

import Post from './post'
import Container from './container'

const Posts = ({ blok: { content } }) => {
  return (
    <StyledContainer marginBottom='200'>
      {content &&
        content.map((post, index) => (
          <Wrapper key={post._uid}>
            <Post key={post._uid} data={post} index={index + 1} />
            <Hr />
          </Wrapper>
        ))}
    </StyledContainer>
  )
}

const Hr = styled.hr`
  border: 1px solid rgba(0, 0, 0, 0.2);
  width: 100%;
  margin: 40px 0;
`
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`
const StyledContainer = styled(Container)`
  flex-direction: column;
  ${Wrapper} {
    &:last-child {
      hr {
        display: none;
      }
    }
  }
`
export default Posts
