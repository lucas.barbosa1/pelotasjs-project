import storyblokClient from '../providers/storyblokClient'

export const getPosts = () =>
  storyblokClient.get('cdn/stories', {
    version: 'draft',
    starts_with: 'home'
  })
