import React, { useState, useEffect } from 'react'

import Components from '../components/components'
import Navbar from '../components/navbar'

const StoryblokEntry = ({ pageContext }) => {
  const [story, setStory] = useState({ content: {} })
  const [globalNavi, setGlobalNavi] = useState({})

  useEffect(() => {
    if (story.uuid === pageContext.story.uuid) {
      return
    }

    setStory({
      ...pageContext.story,
      content: JSON.parse(pageContext.story.content)
    })
  }, [story, pageContext.story])

  useEffect(() => {
    if (globalNavi.uuid === pageContext.globalNavi.uuid) {
      return
    }

    setGlobalNavi({
      ...pageContext.globalNavi,
      content: JSON.parse(pageContext.globalNavi.content)
    })
  }, [globalNavi, pageContext.globalNavi])

  return (
    <div>
      {globalNavi.content && <Navbar blok={globalNavi.content.body[0]} />}
      {React.createElement(Components(story.content.component), {
        key: story.content._uid,
        blok: story.content
      })}
    </div>
  )
}

export default StoryblokEntry
